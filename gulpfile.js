const {src, dest} = require('gulp')
const sass = require('gulp-sass')
const csso = require('gulp-csso')
const include = require('gulp-file-include')
const htmlmin = require('gulp-htmlmin')
const del = require('del')
const concat = require('gulp-concat')
const autoprefixer = require('gulp-autoprefixer')
const sync = require('browser-sync').create() //чтобы browser-sync сразу работал

function html() {
    return src('src/**.html') //метод src вернет некоторым стрим, у которого будет доступен метод pipe()
        .pipe(include({
            prefix: '@@'
        }))
        .pipe(dest('dist'))
}

function scss(){
    return src('src/scss/**.scss')
        .pipe(sass())
        .pipe(autoprefixer({
            browsers: ['last 2 versions']
        }))
        .pipe(csso())
        .pipe(concat('index.css'))
        .pipe(dest('dist'))
}

exports.html = html
exports.scss = scss